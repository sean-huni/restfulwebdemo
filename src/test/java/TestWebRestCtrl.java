import com.site.service.impl.LoginDetailsService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;

/**
 * PROJECT   :RestWebDemo
 * PACKAGE   :PACKAGE_NAME
 * USER      :s34n
 * DATE      :2016/03/20
 * TIME      :1:14 AM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath*:xml/app-context.xml",
        "classpath*:xml/auth-security.xml",
        "classpath*:xml/filter-services.xml",
        "classpath*:xml/mvc-dispatcher-servlet.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestWebRestCtrl {

    @Autowired
    LoginDetailsService loginDetailsService;

    @Before
    public void setup() {
        System.out.println("*****************************Before********************************");
    }

    @Test
//    @Ignore
    public void a_test() {
        loginDetailsService.loadUserByUsername("email1@companyOne.com");
    }

    /**
     * Must pass the test for the user that doesn't exist in the system.
     */
    @Test(expected = UsernameNotFoundException.class)
    public void b_test() {
        loginDetailsService.loadUserByUsername("email@doesNotExist.com");
    }

}
