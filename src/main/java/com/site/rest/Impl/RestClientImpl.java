package com.site.rest.Impl;

import com.site.model.GroupAuthorities;
import com.site.model.GroupMembers;
import com.site.model.User;
import com.site.rest.RestClient;
import com.site.utils.EncryptPass;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Project: RestWebDemo.
 * Created: s34n.
 * Date: 01-03-2016
 * Time: 10:32
 */
@Service
public class RestClientImpl implements RestClient {
    private static final String REST_SERVICE_URI = "http://localhost:8080/RestfulDemo";
    private static final String REST_USER_URI = "/user/login",
            VARIABLE_DELIMITER = "?", //Variable delimiter.
            PARAMETER_DELIMITER = "&";    //Parameter demilimeter.
    private Logger log = Logger.getLogger(RestClientImpl.class.getName());
    private RestTemplate restTemplate = new RestTemplate();

    //Example with String Token.
    public String doRequest(String username) throws Exception {
        String url = REST_SERVICE_URI + REST_USER_URI;
        String securityToken;

//        securityToken = new TokenGen().generateTokenKey();
//        Load a proper Token
        securityToken = "njp3ran1lnoqrbahe5efv7ko7vpdep2vt2j86qvt3feha5rga818ielc2b8ak37sna5e0t5s2s2stcdg4rfn3a1mm7g0dk2b755mg93vgk1qipnq10f5scirs30p1r3do73rpqtjttl952vbrmp9gou7p1a7293oqckj4donujq287iiiisua080fbheoua2shj1jvh0145pu10v";

        url += "/{username}";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("username", username);
        params.put("token", securityToken);
        return doRequest(url, HttpMethod.GET, params, null);
    }


    private String doRequest(String url, HttpMethod method, Map<String, Object> params, String body) {
        RestTemplate restTemplate = new RestTemplate();

        log.info("[" + method.toString() + "] " + url + " - " + params);
        return restTemplate.getForObject(url, String.class, params);
    }


    @Override
    public User findUser(Boolean aBoolean, String username) throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        String paramUsername = "username=" + username;


        User obj = restTemplate.getForObject(REST_SERVICE_URI +
                REST_USER_URI + "/findUser" + VARIABLE_DELIMITER + paramUsername, User.class);
        System.out.println(obj.toString());
        return obj;
    }

    @Override
    public List<GroupMembers> findGroupMembers(String username) throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        String paramUsername = "username=" + username;

        List<GroupMembers> obj = restTemplate.getForObject(REST_SERVICE_URI +
                REST_USER_URI + "/findGroupMembers" + VARIABLE_DELIMITER + paramUsername, List.class);
        System.out.println(obj);
        return obj;
    }

    public List<GroupAuthorities> findGroupAuthorities(List<GroupMembers> groupMembersList) throws Exception {
        RestTemplate restTemplate = new RestTemplate();


        Map<String, Object> params = new HashMap<String, Object>();
        params.put("members", groupMembersList);

        String requestUrl = REST_SERVICE_URI + "/user/login/findGroupAuthorities" + VARIABLE_DELIMITER;

        if (!requestUrl.contains("?")) {
            requestUrl = requestUrl + "?";
        }
        String paramString = "";
        for (String paramName : params.keySet()) {
            paramString = paramString + paramName + "={" + paramName + "}&";
        }
        paramString = paramString.substring(0, paramString.length() - 1);
        requestUrl = requestUrl + paramString;

        if (!params.isEmpty()) {
            for (String key : params.keySet()) {
                Object value = params.get(key);
                if (value instanceof List) {
                    List list = (List) value;
                    String listString = list.toString().replace("[", "").replace("]", "").replace(" ", "");
                    params.put(key, listString);
                }
            }
        }

        log.info(params.toString());
        List<GroupAuthorities> obj = restTemplate.getForObject(requestUrl, List.class, params);
        System.out.println(obj);
        return obj;
    }
}
