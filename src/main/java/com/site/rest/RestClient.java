package com.site.rest;

import com.site.model.GroupAuthorities;
import com.site.model.GroupMembers;
import com.site.model.User;

import java.util.List;

/**
 * Project: RestWebDemo.
 * Created: s34n.
 * Date: 01-03-2016
 * Time: 11:32
 */
public interface RestClient {

    User findUser(Boolean aBoolean, String username) throws Exception;

    List<GroupMembers> findGroupMembers(String username) throws Exception;

    List<GroupAuthorities> findGroupAuthorities(List<GroupMembers> groupMembers) throws Exception;
}
