package com.site.controller;

import com.site.rest.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

/**
 * Project: RestWebDemo.
 * Created: s34n.
 * Date: 01-03-2016
 * Time: 10:20
 */

@Controller
@RequestMapping(value = {"/", "/user**"})
public class WebCtrl {
    private static final String devComp = "Sean Huni";
    private static final Date date = new Date();
    private static final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-YYYY HH:mm:ss");
    private final static Logger log = Logger.getLogger(WebCtrl.class.getName());
    private final static String strDate = dateFormat.format(date);

    @Autowired
    RestClient restClient;

//    "email1@companyOne.com", MD5("password1")
//    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    public ModelAndView loginResp(@RequestParam(value="username", required = true) String username,
//                     @RequestParam(value="password", required = true) String password){
//        System.out.println("Worked!!!");
//        log.info("Worked!!!");
//        ModelAndView mv = new ModelAndView();
//        mv.addObject("title", "Fault Login System");
//        mv.addObject("subtitle", "Login Page");
//        mv.addObject("ownerName", devComp);
//        mv.addObject("nowDate", strDate);
//        String status="";
//
//        try {
//            status = restClient.doRequest(username, password);
//        } catch (Exception e) {
//            mv.addObject("error", e.getMessage());
//            e.printStackTrace();
//        } finally {
//            password = null;
//            if(!status.equalsIgnoreCase("success")){
//                mv.addObject("error", "Invalid username and password!");
//                mv.setViewName("login");
//                return mv;
//            }
//
//            mv.addObject("message", username+" is successfully logged-in.");
//            mv.setViewName("accounts/landingPage");
//            return mv;
//        }
//
//    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginReq(@RequestParam(value = "error", required = false) Boolean error,
                                 @RequestParam(value = "logout", required = false) Boolean msg) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("title", "Fault Login System");
        mv.addObject("subtitle", "Login Page");
        mv.addObject("ownerName", devComp);
        mv.addObject("nowDate", strDate);

        if (msg != null && msg) {
            mv.addObject("msg", "You've been logged out successfully.");
            error = null;
        } else if (error != null && error) {
            mv.addObject("error", "Invalid username and password!");
            msg = null;
        }

        mv.setViewName("login");
        return mv;
    }

    /**
     * Default private authenticated page.
     * The contents to be displayed to the users currently logged into the system.
     *
     * @return ModelAndView
     */
    @RequestMapping(value = "/landingPage", method = RequestMethod.GET)
    public ModelAndView landingPage() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName(); //get logged in username

        ModelAndView mv = new ModelAndView();

//        String bodyTiltle = "Managing & Tracking your issues smoothly",
//        body = "";

        Integer iCount = 0;
        final Map map = new TreeMap<Integer, String>();
        map.put(iCount, "Faults are logged instantly.");
        iCount++;
        map.put(iCount, "Faults traced & streamlined seamlessly to the affected communities.");
        iCount++;
        map.put(iCount, "Announcement from the community representatives.");
        iCount++;
        map.put(iCount, "Fault management supports transparent visibility.");
        iCount++;
        map.put(iCount, "Community members are able to send & receive messages to/from the Community-Rep.");

        mv.addObject("title", "Fault Login System");
        mv.addObject("subtitle", "Welcome to the Fault-Logging System");
        mv.addObject("message", "successfully logged-in as: " + username);
        mv.addObject("homePageLoginReq", "Click here to log-in.");
        mv.addObject("bodyBullets", map);
        mv.addObject("slideshow", "Logged Faults Snapshots");
        mv.addObject("happyMember", "Shows logged-fault status");
        mv.addObject("unhappyMember", "Streamlined Comm-Rep posts.");
        mv.addObject("ownerName", devComp);
        mv.addObject("nowDate", strDate);

        mv.setViewName("accounts/landingPage");
        return mv;
    }


    /**
     * Default public landing page.
     *
     * @return
     */
    @RequestMapping(value = {"/index"}, method = RequestMethod.GET)
    public ModelAndView homePage() {

        ModelAndView mv = new ModelAndView();
        mv.addObject("title", "Fault Login System - Home Page");
        mv.addObject("subtitle", "Welcome to the fault logging system");
        mv.addObject("message", "A message to all users of the system. You're currently view the site as an anonymous user.");
        mv.addObject("homePageLoginReq", "Please click here to login.");
        mv.addObject("ownerName", devComp);
        mv.addObject("nowDate", strDate);

        mv.setViewName("index");
        return mv;
    }

    /**
     * Redirects the user to the login page should the user call the direct link of the site.
     *
     * @return redirect link.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String redirectPage() {
        return "redirect:/login";
    }
}