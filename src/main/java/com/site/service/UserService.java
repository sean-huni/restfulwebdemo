package com.site.service;

import com.site.model.User;

/**
 * Project: RestWebDemo.
 * Created: s34n.
 * Date: 06-03-2016
 * Time: 13:46
 */
public interface UserService{
    public User findByUsername(String username) throws Exception;
    public User findByUsernamePassword(String username, String password) throws Exception;
}
