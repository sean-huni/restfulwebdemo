package com.site.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.site.model.GroupAuthorities;
import com.site.model.GroupMembers;
import com.site.model.User;
import com.site.rest.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * PROJECT   :RestWebDemo
 * PACKAGE   :com.site.service.impl
 * USER      :s34n
 * DATE      :2016/03/16
 * TIME      :12:16 PM
 */
public class LoginDetailsService implements UserDetailsService {
    private final static Logger log = Logger.getLogger(LoginDetailsService.class.getName());

    @Autowired
    private RestClient restClient;

    /**
     * Locates the user based on the username. In the actual implementation, the search
     * may possibly be case sensitive, or case insensitive depending on how the
     * implementation instance is configured. In this case, the <code>UserDetails</code>
     * object that comes back may have a username that is of a different case than what
     * was actually requested..
     *
     * @param username the username identifying the user whose data is required.
     * @return a fully populated user record (never <code>null</code>)
     * @throws UsernameNotFoundException if the user could not be found or the user has no
     *                                   GrantedAuthority
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = new User();
        try {
            if (username == null || username.equals("")) {
                throw new RuntimeException("Username and Password not supplied.");
            }
            System.out.println("Username: " + username);
            user = restClient.findUser(true, username); // Return an object
            if (user != null) {
                log.info(user.toString());
            } else if (user == null || user.getPk() == null) {
                log.severe("The User object is null." +
                        "\nMethod executed: restClient.findUser(true, username);");
                throw new RuntimeException("System experienced a critical error!");
            }
        } catch (NoResultException e) {
            throw new UsernameNotFoundException(username + " Username not found.");
        } catch (Exception e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        }

        System.out.println("Class: LoginDetailsService.\nMethod: loadUserByUsername.\nVariable User: " + (user != null ? user.toString() : null));

        if (user == null || user.getPk() == null) {
            log.warning(username + " username not found.");
            throw new UsernameNotFoundException(username + " username not found.");
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                user.getEnabled(), true, true, true, getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        try {
            List<GroupMembers> groupMembers = restClient.findGroupMembers(user.getUsername());

            for (GroupAuthorities userAuth : decodeHashMap(restClient.findGroupAuthorities(groupMembers))) {
                System.out.println("User Authority: " + userAuth.toString());
                authorities.add(new SimpleGrantedAuthority("ROLE_" + userAuth.getAuthority()));
            }
        } catch (Exception e) {
            log.severe(e.getMessage());
            System.out.println("\n\n*********\nClass: LoginDetailsService.\nMethod: getGrantedAuthorities.\n\n*********\n\n");
            e.printStackTrace();
        }
        System.out.print("Authorities :" + authorities);
        return authorities;
    }


    private List<GroupAuthorities> decodeHashMap(List<GroupAuthorities> authorities) {
        List<GroupAuthorities> authoritiesArrayList = new ArrayList<GroupAuthorities>();
        ObjectMapper mapper = new ObjectMapper();

        try {
            List<GroupAuthorities> groupAuthorities = mapper.convertValue(authorities,
                    new TypeReference<List<GroupAuthorities>>() {
                    });

            authoritiesArrayList.addAll(groupAuthorities);
        } catch (IllegalArgumentException e) {
            System.out.println("\n\n*********\nClass: LoginDetailsService.\nMethod: decodeHashMap.\n\n*********\n\n");
            e.printStackTrace();
        }
        return authoritiesArrayList;
    }
}
