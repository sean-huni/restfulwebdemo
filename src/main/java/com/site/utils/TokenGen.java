package com.site.utils;

import java.security.SecureRandom;
import java.math.BigInteger;

public final class TokenGen {
  private SecureRandom random = new SecureRandom();

  public String generateTokenKey() {
    return new BigInteger(1040, random).toString(32);
  }

//  public static void main(String[] args) {
//    String key = new TokenGen().generateTokenKey();
//
//      System.out.println("Key: "+key+"\nLength: "+key.length());
//  }
}