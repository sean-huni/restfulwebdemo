package com.site.utils;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;


/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 01-03-2016
 * Time: 09:09
 */
public class EncryptPass extends Md5PasswordEncoder{

    public String md5Encrypt(String md5){
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        return encoder.encodePassword(md5, null);
    }
}
