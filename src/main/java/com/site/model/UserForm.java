package com.site.model;

import com.site.utils.EncryptPass;

/**
 * Project: RestWebDemo.
 * Created: s34n.
 * Date: 03-03-2016
 * Time: 08:22
 */
public class UserForm {
    private String username, password, userRole, errorMessage;
    private Boolean isError;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = new EncryptPass().md5Encrypt(username);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getError() {
        return isError;
    }

    public void setError(Boolean error) {
        isError = error;
    }
}
