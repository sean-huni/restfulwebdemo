package com.site.model.impl;

import com.site.model.AuthUser;

/**
 * Project: RestWebDemo.
 * Created: s34n.
 * Date: 04-03-2016
 * Time: 08:12
 */
public class AuthUserImpl implements AuthUser {
    private String username, name, surname, password, token;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
