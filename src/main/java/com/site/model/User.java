package com.site.model;


//import org.hibernate.jpamodelgen.xml.jaxb.GenerationType;
//import org.springframework.data.annotation.Id;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Table;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 29-02-2016
 * Time: 16:29
 */
public class User {

    private Integer pk;
    private String username;
    private String password;
    private Boolean enabled;

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "User [pk:" + pk + ", Username:" + username + ", Password=" + password + ", Enabled:" + enabled + "]";
    }
}
