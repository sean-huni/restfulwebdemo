package com.site.model;

import javax.persistence.*;

/**
 * PROJECT   :RestfulDemo
 * PACKAGE   :com.demo.model
 * USER      :s34n
 * DATE      :2016/03/14
 * TIME      :10:39 AM
 */

@Entity
@Table(name = "groups")
public class Groups {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer pk;
    @Column(name = "group_name")
    private Integer groupName;

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public Integer getGroupName() {
        return groupName;
    }

    public void setGroupName(Integer groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "[pk: " + pk + ", groupName: " + groupName + "]";
    }
}
