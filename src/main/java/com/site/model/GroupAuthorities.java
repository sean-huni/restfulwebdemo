package com.site.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * PROJECT   :RestfulDemo
 * PACKAGE   :com.site.model
 * USER      :s34n
 * DATE      :2016/03/14
 * TIME      :10:37 AM
 */
@Entity
@Table(name = "group_authorities")
public class GroupAuthorities {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer pk;
    @Column(name = "group_id")
    private Integer groupId;
    @Column(name = "authority")
    private String authority;

    //    @ManyToMany(mappedBy="groupAuthorities")
    @Transient
    private Set<User> users = new HashSet<User>();

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupPk) {
        this.groupId = groupPk;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public String toString() {
        return "User [pk:" + pk + ",groupID:" + groupId + ",authority=" + authority + "]";
    }
}
