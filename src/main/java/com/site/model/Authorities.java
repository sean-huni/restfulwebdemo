package com.site.model;

import javax.persistence.*;

/**
 * PROJECT   :RestfulDemo
 * PACKAGE   :com.demo.model
 * USER      :s34n
 * DATE      :2016/03/14
 * TIME      :10:40 AM
 */
@Entity
@Table(name = "authorities")
public class Authorities {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk")
    private Integer pk;
    @Column(name = "username")
    private String username;
    @Column(name = "authority")
    private String authority;

    //    @ManyToOne
    @Transient
    private User user;

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "User [pk:" + pk + ",username=" + username + ",authority=" + authority + "]";
    }
}
