package com.site.model;

/**
 * Project: RestWebDemo.
 * Created: s34n.
 * Date: 04-03-2016
 * Time: 08:27
 */
public interface AuthUser {

    String getUsername();
    void setUsername(String username);
    String getName();
    void setName(String name);
    String getSurname();
    void setSurname(String surname);
    String getPassword();
    void setPassword(String password);
    String getToken();
    void setToken(String token);
}
