<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: s34n
  Date: 2016/03/19
  Time: 8:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<html--%>
<%--&lt;%&ndash;<jsp:directive.page contentType="text/html;charset=UTF-8"/>&ndash;%&gt;--%>
<%--xmlns:jsp="http://java.sun.com/JSP/Page"--%>
<%--version="2.1"--%>
<%--xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"--%>
<%--xsi:schemaLocation="http://java.sun.com/jsp/jstl/core">--%>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="brand" href="<c:url value='/user/index'/>">Fault Logging System</a>
        <div class="nav-collapse collapse">
            <ul class="nav">
                <li><a href="<c:url value='/user/index'/>">Home</a></li>
                <li><a href="<c:url value='/user/login'/>">Login</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">What we offer<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#services">Services</a></li>
                        <li><a href="#products">Products</a></li>
                    </ul>
                </li>
                <%--<li><a href="#compPort">Portfolio</a></li>--%>
                <li><a href="#theSite">Tutorial</a></li>
                <li><a href="#team">About Us</a></li>
                <li><a href="#contactUs">Contact Us</a></li>
            </ul>
            <%--<form class="navbar-form pull-right"
                  action="<c:url value='/j_spring_security_check' />" method='POST'>
                <input class="span2" type="text" placeholder="Username" name="username">
                <input class="span2" type="password" placeholder="Password" name="password">
                <button type="submit" name="submit" value="submit" class="btn">Login</button>
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
            </form>--%>
        </div><!--/.nav-collapse -->
    </div>
</nav>


<%--</html>--%>