<%--
  Created by IntelliJ IDEA.
  User: s34n
  Date: 2/7/16
  Time: 5:16 PM
  To change this template use File | Settings | File Templates.
--%>
<footer class="modal-footer">
    <div class="pull-left">
        <p>&copy; Developed by: ${ownerName}. Last updated: ${nowDate}</p>
    </div>

    <div class="pull-right"><a href="#">Back to top</a></div>
</footer>