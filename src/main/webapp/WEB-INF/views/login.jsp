<%--
  Created by IntelliJ IDEA.
  User: s34n
  Date: 3/1/16
  Time: 10:19 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="xsi" uri="http://java.sun.com/jsp/jstl/core" %>

<html
<%--<jsp:directive.page contentType="text/html;charset=UTF-8"/>--%>
        xmlns:jsp="http://java.sun.com/JSP/Page"
        version="2.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://java.sun.com/jsp/jstl/core">
<head>
    <title>${title}</title>

    <%--Importing CSS Files--%>
    <link href="<c:url value='/viewResources/bootstrap/css/bootstrap.css'/>" rel="stylesheet">
    <link href="<c:url value='/viewResources/bootstrap/css/bootstrap-responsive.css'/>" rel="stylesheet">
    <link href="<c:url value='/viewResources/css/bootstrap-theme.css'/>" rel="stylesheet">
    <link href="<c:url value='/viewResources/css/login.css'/>" rel="stylesheet">

    <%--Importing Js Files--%>
    <script src="<c:url value='/viewResources/js/jquery-2.2.0.js'/>"></script>
    <%-- replace with production-ready compressed jquery-2.2.0.min.js--%>
    <script src="<c:url value='/viewResources/js/jquery.hotkeys.js'/>"></script>
    <script src="<c:url value='/viewResources/js/jquery.form.js'/>"></script>
    <script src="<c:url value='/viewResources/bootstrap/js/bootstrap.js'/>"></script>

</head>
<body>
<%--Navigation--%>
<%--<jsp:include page="generic/navi.jsp"/>--%>
<%@ include file="generic/navi.jsp" %>

<%--Dashboard--%>

<%--Logout Script--%>
<c:if test="${pageContext.request.userPrincipal.name != null}">
    <!-- For user that is currently logged in-->
    <c:url value="/j_spring_security_logout" var="logoutUrl"/>
    <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>

</c:if>


<%--Body--%>
<div class="container page-header" role="main">
    <div id="login-box">
        <h3 class="accordion-heading">User Authentication</h3>
        <c:if test="${pageContext.request.userPrincipal.name == null}">

            <c:if test="${not empty error}">
                <div class="alert alert-error">${error}</div>
            </c:if>
            <c:if test="${not empty msg}">
                <div class="alert alert-success">${msg}</div>
            </c:if>

            <%--<font color="red">${message}</font>--%>
            <form name='loginForm' action="<c:url value='/j_spring_security_check' />" method='POST' role="form">
                <table>
                    <tr>
                        <td>Username:</td>
                        <td><input type='text' name='username' required></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type='password' name='password' required/></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a type="button" class="btn btn-warning" id="forget-password" name="forget-password"
                               data-toggle="modal" data-target="#forgotPassword">Forgot password ?</a>
                            <input class="input btn btn-info" name="submit" type="submit" value="Login"/>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>

        </c:if>

        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <script src="<c:url value='/viewResources/js/utils/user.js'/>"></script>
            <%--<script src="<%@ include file="/viewResources/js/utils/user.js" %>" ></script>--%>
            Currently logged-in as: ${pageContext.request.userPrincipal.name} | <a
                href="javascript:formSubmit()"> Logout</a>
        </c:if>
    </div>
</div>

<%--Footer--%>
<jsp:include page="generic/footer.jsp"/>

</body>
</html>
