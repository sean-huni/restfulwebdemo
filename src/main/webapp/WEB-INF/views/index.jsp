<%--
  Created by IntelliJ IDEA.
  User: s34n
  Date: 3/1/16
  Time: 10:19 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html
        <jsp:directive.page contentType="text/html;charset=UTF-8"/>
        xmlns:jsp="http://java.sun.com/JSP/Page"
        version="2.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://java.sun.com/jsp/jstl/core">
<head>
    <title>${title}</title>

    <%--Importing CSS Files--%>
    <link href="<c:url value='/viewResources/bootstrap/css/bootstrap.css'/>" rel="stylesheet">
    <link href="<c:url value='/viewResources/bootstrap/css/bootstrap-responsive.css'/>" rel="stylesheet">
    <link href="<c:url value='/viewResources/css/bootstrap-theme.css'/>" rel="stylesheet">

    <%--Importing Js Files--%>
    <script src="<c:url value='/viewResources/js/jquery-2.2.0.js'/>"></script>
    <%-- replace with production-ready compressed jquery-2.2.0.min.js--%>
    <script src="<c:url value='/viewResources/js/jquery.hotkeys.js'/>"></script>
    <script src="<c:url value='/viewResources/js/jquery.form.js'/>"></script>
    <script src="<c:url value='/viewResources/bootstrap/js/bootstrap.js'/>"></script>

</head>
<body>

<%--Navigation--%>
<%--<jsp:include page="generic/navi.jsp"/>--%>
<%@ include file="generic/navi.jsp" %>


<%--Dashboard--%>

<%--Body--%>
<div class="container">
    <div class="temp">
        Subtitle : ${subtitle}
        <br>
        Message : ${message}
        <br>
        <div class="page-information" role="document">
            <c:if test="${pageContext.request.userPrincipal.name == null}">
                <a href="/RestWebDemo-1.0/user/login">${homePageLoginReq}</a>
            </c:if>

            <c:if test="${pageContext.request.userPrincipal.name != null}">
                <script src="<c:url value='/viewResources/js/utils/user.js'/>"></script>
                Welcome : ${pageContext.request.userPrincipal.name} | <a href="javascript:formSubmit()"> Logout</a>
            </c:if>
        </div>
    </div>
</div>

<%--Footer--%>
<jsp:include page="generic/footer.jsp"/>

<c:if test="${pageContext.request.userPrincipal.name != null}">
    <!-- For user that is currently logged in-->
    <c:url value="/j_spring_security_logout" var="logoutUrl"/>
    <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>

</c:if>

</body>
</html>
