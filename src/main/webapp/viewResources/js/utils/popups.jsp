<script>
    /**
     * Shows a modal popup message, using the genericPopup div. A single OK button is presented to the user.
     * @param title     message box title
     * @param message   message content
     */
    function showPopup(title, message) {
        $('#genericPopupTitle').text(title);
        $('#genericPopupMessage').html(message.replace("\n", "<br/>"));

        $('#genericPopupClose').text('OK').addClass('btn-primary').show();

        $('#genericPopupAction').hide();
    }



    /**
     * Shows a modal popup message using the genericPopup div. OK and Cancel buttons are displayed.
     * @param title     message box title
     * @param message   message content
     * @param okEvent   the event to execute when the user pressed the OK button.
     */
    function showPopup(title, message, okEvent) {
        $('#genericPopupTitle').text(title);
        $('#genericPopupMessage').html(message.replace("\n", "<br/>"));

        $('#genericPopupClose').text('Cancel').removeClass('btn-primary');

        $('#genericPopupAction').unbind('click').click(okEvent).addClass('btn-primary').show();

        $('#genericPopup').modal('show');
    }

    /**
     * Shows an error message popup. Box will have a red title.
     * @param title     message box title
     * @param message   message content
     * @param details   optionally, an array of error message details
     */
    function showErrorPopup(title, message, details) {
        $('#genericErrorTitle').text(title);
        $('#genericErrorMessage').html(message != null ? message.replace("\n", "<br/>") : '');
        if (details != null) {
            var detailList = "";
            $.each(details, function (a, b) {
                detailList += "<li>" + b.error + "</li>";
            });
            $('#genericErrorDetails').html("<ul>" + detailList + "</ul>");
        } else {
            $('#genericErrorDetails').text('');
        }

        showErrorFields(details);

        $('#genericErrorPopup').modal('show');
    }


    /**
     * Shows an error message popup. Box will have a green title.
     * @param title     message box title
     * @param message   message content
     * @param details   optionally, an array of error message details
     */
    function showSuccessPopup(title, message, details) {
        $('#genericSuccessTitle').text(title);
        $('#genericSuccessMessage').html(message != null ? message.replace("\n", "<br/>") : '');
        if (details != null) {
            var detailList = "";
            $.each(details, function (a, b) {
                detailList += "<li>" + b.error + "</li>";
            });
            $('#genericSuccessDetails').html("<ul>" + detailList + "</ul>");
        } else {
            $('#genericSuccessDetails').text('');
        }

        $('#genericSuccessPopup').modal('show');
    }

    /**
     * Uses an existing element to show an error message, usually a div with class "alert-error" assigned.
     * @param responseObj the error response object received form the server
     * @param errorDiv the name of the div to populate with error details
     */
    function showErrorInline(responseObj, errorDiv) {
        var error = responseObj.message;
        if (responseObj.errors != null) {
            error += "<ul>";
            for (var i = 0; i < responseObj.errors.length; i++) {
                error += "<li>" + responseObj.errors[i].error + "</li>";
            }
            error += "</ul>";

            showErrorFields(responseObj.errors);
        }

        $("#" + errorDiv).find("div").first().html(error);
        $("#" + errorDiv).show();
        $("#" + errorDiv).parent().animate({scrollTop:0}, 'fast');
    }

    function showProgress(percentage) {
        if (percentage==undefined){
            $("#modalProgressBar").width("100%");
        }else{
            $("#modalProgressBar").width(percentage + "%");
        }
        $("#modalProgressMessage").text("");
        $("#popupWait").modal('show');
    }

    function hideProgress() {
        $("#modalProgressMessage").text("");
        $("#popupWait").modal('hide');
    }

    function setProgress(progress, message){
        $("#modalProgressBar").width(progress+"%");
        if (message!=undefined){
            $("#modalProgressMessage").text(message);
        }
    }


    function showErrorFields(errors) {
        $('.nav-tabs a .badge').remove();

        if (errors != null && errors.length > 0) {
            var tabCounter = {};
            for (var i = 0; i < errors.length; i++) {
                $('#' + errors[i].controlGroup).addClass('error');

                var tab = $('#' + errors[i].controlGroup).parents('div.tab-pane');
                if (tab != null) {
                    if (!(tab.attr('id') in tabCounter)) {
                        tabCounter[tab.attr('id')] = 0;
                    }
                    tabCounter[tab.attr('id')]++;
                }
            }
            $.each(tabCounter, function (id, count) {
                $('a[href$="#' + id + '"]').text($('a[href$="#' + id + '"]').text() + ' ');
                $('<span/>').addClass('badge').addClass('badge-important').text(count).appendTo($('a[href$="#' + id + '"]'));
            });
        }
    }
</script>

<div class="modal hide fade" id="genericPopup" style="min-width: 400px;">
    <div class="modal-header btn-info">
        <button class="close" data-dismiss="modal">x</button>
        <h3 id="genericPopupTitle">Message</h3>
    </div>
    <div class="modal-body">
        <p id="genericPopupMessage">Information</p>
    </div>
    <div class="modal-footer">
        <button id="genericPopupAction" class="btn" data-dismiss="modal" aria-hidden="true"><i
                class="icon-ok icon-white"></i> OK
        </button>
        <button id="genericPopupClose" class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i>
            Cancel
        </button>
    </div>
</div>

<div class="modal hide fade" id="genericErrorPopup" style="min-width: 400px;">
    <div class="modal-header btn-danger">
        <button class="close" data-dismiss="modal">x</button>
        <h3 id="genericErrorTitle">Error</h3>
    </div>
    <div class="modal-body">
        <p id="genericErrorMessage">An error happened.</p>

        <p id="genericErrorDetails">&nbsp</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-ok icon-white"></i> Close
        </button>
    </div>
</div>

<div class="modal hide fade" id="genericSuccessPopup" style="min-width: 400px;">
    <div class="modal-header btn-success">
        <button class="close" data-dismiss="modal">x</button>
        <h3 id="genericSuccessTitle">Error</h3>
    </div>
    <div class="modal-body">
        <p id="genericSuccessMessage">An error happened.</p>

        <p id="genericSuccessDetails">&nbsp</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-success" data-dismiss="modal" aria-hidden="true"><i class="icon-ok icon-white"></i> OK
        </button>
    </div>
</div>

<div class="modal hide" id="popupWait" style="min-width: 400px; min-height: 300px">
    <h3 style="margin: 100px 50px 0 50px;">Please Wait</h3>

    <div class="progress progress-striped active" style="margin: 0 50px 0 50px">
        <div id="modalProgressBar" class="bar" style="width: 100%;">
            <span class="sr-only"></span>
        </div>
    </div>

    <div class="modal-body span5">
        <p id="modalProgressMessage">&nbsp</p>
    </div>
</div>